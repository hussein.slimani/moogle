package moogle;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class MoogleCore {

    // REGEX for extracting words from a sentence
    private static final String REGEX = "[^\\p{L}\\p{N}\\p{Sc}]+";

    // To store the lines (value + index in file), I'm using a map instead of a simple array to avoid having empty array indexes (could be dumb)
    private final Map<Integer, String> lines;

    // Using a HashMap to store the index
    private final Map<String, Set<Integer>> wordsIndex;

    MoogleCore() {

        // Instantiating the index map, it will be populated in the readFile function
        this.wordsIndex = new HashMap<>();
        this.lines = readFile();
    }

    private Map<Integer, String> readFile() {

        Map<Integer, String> iLines = new HashMap<>();

        try {

            // Get the class loader to access the input file and feed it to a Scanner
            ClassLoader classLoader = getClass().getClassLoader();
            Scanner fileScanner = new Scanner(new File(Objects.requireNonNull(classLoader.getResource("paris.txt")).getFile()));

            // Instantiating the loop variables
            String currentLine;
            int lineNumber = 1;

            while (fileScanner.hasNextLine()) {

                // read the next line
                currentLine = fileScanner.nextLine().trim();

                // Process it only if it is not empty (trim to get rid of some lines containing only whitespaces)
                if (!currentLine.isEmpty()) {

                    // We populate the line map
                    iLines.put(lineNumber, currentLine);

                    // We split the line into words
                    String[] distinctWords = currentLine.split(REGEX);

                    // Iterate through the words to populate the index
                    for(String word : distinctWords) {

                        // We normalize the word (remove accents and put in all lower-case)
                        word = this.normalize(word);

                        // Populate the index with the word and associated line number
                        this.wordsIndex.computeIfAbsent(word, k -> new HashSet<>()).add(lineNumber);
                    }
                }

                // Increment the line counter
                lineNumber++;
            }
        } catch (FileNotFoundException e) {

            throw new RuntimeException(e);
        }

        return iLines;
    }

    public Map<ArrayList<String>, TreeSet<Integer>> find(String userQuery) {

        Map<Integer, ArrayList<String>> queryResults;

        queryResults = Arrays
            .stream(userQuery.trim().split(REGEX)) // Split and stream
            .map(this::normalize) // Normalize the query to match the index
            .filter(this.wordsIndex::containsKey) // Filter out words not included in index
            // Custom collector to build our result array
            .collect(HashMap::new, // Supplier function gives us a new HashMap
                    // Accumulator function builds the hashmap
                    (elMap, word) -> this.wordsIndex.get(word).forEach(
                            lineIndex -> elMap.computeIfAbsent(lineIndex, k -> new ArrayList<>()).add(word)
                    ),
                    Map::putAll); // Combiner function -- not actually used

        return groupByWords(queryResults);
    }

    private Map<ArrayList<String>, TreeSet<Integer>> groupByWords(Map<Integer, ArrayList<String>> queryResults) {

        return queryResults.
                keySet(). // Pull Keys from the HashMap
                stream(). // Stream
                // Group the keys by the value associated in queryResults (i.e. all lines containing the same set of words will end up grouped together)
                collect(Collectors.groupingBy(queryResults::get, Collectors.toCollection(TreeSet::new)));
    }

    public String getLine(int lineIndex) {

        return this.lines.get(lineIndex);
    }

    private String normalize(String word) {

        // I bent the rules a lil bit and used an outside library to strip the accents from words
        return StringUtils.stripAccents(word).toLowerCase();
    }
}
