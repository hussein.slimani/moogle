package moogle;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeSet;

public class MoogleQuery {

    private static final MoogleCore moogle = new MoogleCore();

    public void query(String userQuery) {

        System.out.printf("User query is: %s\n", userQuery);

        Map<ArrayList<String>, TreeSet<Integer>> searchResults = moogle.find(userQuery);

        // We print the results for each line
        searchResults.forEach((words, lines) -> {

            System.out.println("------------------------------------------------------------");
            System.out.printf("The %s been found at the following locations:\n\n", words.size() > 1 ? "words " + words + " have" : "word " + words + " has");
            lines.forEach(lineIndex -> System.out.printf("Line %d: %s\n", lineIndex, moogle.getLine(lineIndex)));
            System.out.println("------------------------------------------------------------");
            System.out.println();
        });

    }
}
