package moogle;

import java.util.Scanner;

public class Moogle {

    private static final MoogleQuery moogleQ = new MoogleQuery();

    public static void main(String[] args) {

        Scanner userInputScanner = new Scanner(System.in);

        System.out.println("------------------------------------------------------------");
        System.out.println("Welcome to Moogle! Please input your search queries below :)");
        System.out.println(" Your query should be a set of words, separated by spaces   ");
        System.out.println("              Type \"exit\" to leave :(                     ");
        System.out.println("------------------------------------------------------------");

        while(true) {

            System.out.print("query--> ");

            String userQuery = userInputScanner.nextLine();

            if ("exit".equalsIgnoreCase(userQuery))
                break;

            moogleQ.query(userQuery);

            System.out.println();
        }
    }
}
